/*
 * luxor-font-private.h: A font
 *
 * Copyright © 2018 Bryce Harrington
 *
 * Released under mit, read the file 'COPYING' for more information.
 *
 * Contributors:
 */

/*
 * This file defines ...
 * It is based on ...
 * It is used by ...
 */
#ifndef LUXOR_FONT_H
#define LUXOR_FONT_H

/**
 * luxor_font_t:
 *
 * Since: 0.0
 */
typedef struct _luxor_font {
  // TODO
} luxor_font_t;

#endif /* LUXOR_FONT_H */
