/*
 * luxor-glyph-private.h: A glyph
 *
 * Copyright © 2018 Bryce Harrington
 *
 * Released under mit, read the file 'COPYING' for more information.
 *
 * Contributors:
 */

/*
 * This file defines ...
 * It is based on ...
 * It is used by ...
 */
#ifndef LUXOR_GLYPH_H
#define LUXOR_GLYPH_H

/**
 * luxor_glyph_t:
 *
 * Since: 0.0
 */
typedef struct _luxor_glyph {
  // TODO
} luxor_glyph_t;

#endif /* LUXOR_GLYPH_H */
