/*
 * luxor-glyph.c: A glyph
 *
 * Copyright © 2018 Bryce Harrington
 *
 * Released under mit, read the file 'COPYING' for more information.
 *
 * Contributors:
 */

/*
 * This file defines ...
 * It is based on ...
 * It is used by ...
 */
/**
 * SECTION:luxor-glyph
 * @title: 
 * @short_description:  type
 *
 * #luxor_glyph_t is a type representing...
 */

#include "luxor-glyph-private.h"

/**
 * luxor_glyph_create:
 *
 * Allocates a new #luxor_glyph_t.
 *
 * The contents of the returned glyph are undefined.
 *
 * @returns The newly allocated glyph, or @c NULL on error.
 *
 * Since: 0.0
 */
luxor_glyph_t *
luxor_glyph_create(void) {
  struct luxor_glyph_t *glyph;

  glyph = calloc(1, sizeof (*glyph));
  if (glyph == NULL)
    return NULL;

  return glyph;
}

/**
 * luxor_glyph_init:
 * @glyph: A #luxor_glyph_t to be initialized.
 *
 * Since: 0.0
 */
void
luxor_glyph_init(luxor_glyph_t *glyph) {
  /* TODO: Initialize the structure's values */
}

/**
 * luxor_glyph_destroy:
 * @glyph: A #luxor_glyph_t to be freed.
 *
 * Frees the resources allocated by luxor_glyph_create().
 *
 * Since: 0.0
 */
void
luxor_glyph_destroy(luxor_glyph_t *glyph) {
  free(glyph);
}

