/*
 * luxor-tesselator.h: A tesselator
 *
 * Copyright © 2018 Bryce Harrington
 *
 * Released under mit, read the file 'COPYING' for more information.
 *
 * Contributors:
 */

/*
 * This file defines ...
 * It is based on ...
 * It is used by ...
 */
#ifndef LUXOR_TESSELATOR_H
#define LUXOR_TESSELATOR_H

luxor_tesselator_t *
luxor_tesselator_create();

void
luxor_tesselator_init(luxor_tesselator_t *tesselator);

void
luxor_tesselator_destroy(luxor_tesselator_t *tesselator);

#endif /* LUXOR_TESSELATOR_H */
