/*
 * luxor-region.c: A region
 *
 * Copyright © 2018 Bryce Harrington
 *
 * Released under mit, read the file 'COPYING' for more information.
 *
 * Contributors:
 */

/*
 * This file defines ...
 * It is based on ...
 * It is used by ...
 */
/**
 * SECTION:luxor-region
 * @title: 
 * @short_description:  type
 *
 * #luxor_region_t is a type representing...
 */

#include "luxor-region-private.h"

/**
 * luxor_region_create:
 *
 * Allocates a new #luxor_region_t.
 *
 * The contents of the returned region are undefined.
 *
 * @returns The newly allocated region, or @c NULL on error.
 *
 * Since: 0.0
 */
luxor_region_t *
luxor_region_create(void) {
  struct luxor_region_t *region;

  region = calloc(1, sizeof (*region));
  if (region == NULL)
    return NULL;

  return region;
}

/**
 * luxor_region_init:
 * @region: A #luxor_region_t to be initialized.
 *
 * Since: 0.0
 */
void
luxor_region_init(luxor_region_t *region) {
  /* TODO: Initialize the structure's values */
}

/**
 * luxor_region_destroy:
 * @region: A #luxor_region_t to be freed.
 *
 * Frees the resources allocated by luxor_region_create().
 *
 * Since: 0.0
 */
void
luxor_region_destroy(luxor_region_t *region) {
  free(region);
}

