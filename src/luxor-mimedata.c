/*
 * luxor-mimedata.c: A mimedata
 *
 * Copyright © 2018 Bryce Harrington
 *
 * Released under mit, read the file 'COPYING' for more information.
 *
 * Contributors:
 */

/*
 * This file defines ...
 * It is based on ...
 * It is used by ...
 */
/**
 * SECTION:luxor-mimedata
 * @title: 
 * @short_description:  type
 *
 * #luxor_mimedata_t is a type representing...
 */

#include "luxor-mimedata-private.h"

/**
 * luxor_mimedata_create:
 *
 * Allocates a new #luxor_mimedata_t.
 *
 * The contents of the returned mimedata are undefined.
 *
 * @returns The newly allocated mimedata, or @c NULL on error.
 *
 * Since: 0.0
 */
luxor_mimedata_t *
luxor_mimedata_create(void) {
  struct luxor_mimedata_t *mimedata;

  mimedata = calloc(1, sizeof (*mimedata));
  if (mimedata == NULL)
    return NULL;

  return mimedata;
}

/**
 * luxor_mimedata_init:
 * @mimedata: A #luxor_mimedata_t to be initialized.
 *
 * Since: 0.0
 */
void
luxor_mimedata_init(luxor_mimedata_t *mimedata) {
  /* TODO: Initialize the structure's values */
}

/**
 * luxor_mimedata_destroy:
 * @mimedata: A #luxor_mimedata_t to be freed.
 *
 * Frees the resources allocated by luxor_mimedata_create().
 *
 * Since: 0.0
 */
void
luxor_mimedata_destroy(luxor_mimedata_t *mimedata) {
  free(mimedata);
}

