/*
 * luxor-glyph.h: A glyph
 *
 * Copyright © 2018 Bryce Harrington
 *
 * Released under mit, read the file 'COPYING' for more information.
 *
 * Contributors:
 */

/*
 * This file defines ...
 * It is based on ...
 * It is used by ...
 */
#ifndef LUXOR_GLYPH_H
#define LUXOR_GLYPH_H

luxor_glyph_t *
luxor_glyph_create();

void
luxor_glyph_init(luxor_glyph_t *glyph);

void
luxor_glyph_destroy(luxor_glyph_t *glyph);

#endif /* LUXOR_GLYPH_H */
