/*
 * luxor-mimedata.h: A mimedata
 *
 * Copyright © 2018 Bryce Harrington
 *
 * Released under mit, read the file 'COPYING' for more information.
 *
 * Contributors:
 */

/*
 * This file defines ...
 * It is based on ...
 * It is used by ...
 */
#ifndef LUXOR_MIMEDATA_H
#define LUXOR_MIMEDATA_H

luxor_mimedata_t *
luxor_mimedata_create();

void
luxor_mimedata_init(luxor_mimedata_t *mimedata);

void
luxor_mimedata_destroy(luxor_mimedata_t *mimedata);

#endif /* LUXOR_MIMEDATA_H */
