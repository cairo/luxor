/*
 * luxor-device.c: A device
 *
 * Copyright © 2018 Bryce Harrington
 *
 * Released under mit, read the file 'COPYING' for more information.
 *
 * Contributors:
 */

/*
 * This file defines ...
 * It is based on ...
 * It is used by ...
 */
/**
 * SECTION:luxor-device
 * @title: 
 * @short_description:  type
 *
 * #luxor_device_t is a type representing...
 */

#include "luxor-device-private.h"

/**
 * luxor_device_create:
 *
 * Allocates a new #luxor_device_t.
 *
 * The contents of the returned device are undefined.
 *
 * @returns The newly allocated device, or @c NULL on error.
 *
 * Since: 0.0
 */
luxor_device_t *
luxor_device_create(void) {
  struct luxor_device_t *device;

  device = calloc(1, sizeof (*device));
  if (device == NULL)
    return NULL;

  return device;
}

/**
 * luxor_device_init:
 * @device: A #luxor_device_t to be initialized.
 *
 * Since: 0.0
 */
void
luxor_device_init(luxor_device_t *device) {
  /* TODO: Initialize the structure's values */
}

/**
 * luxor_device_destroy:
 * @device: A #luxor_device_t to be freed.
 *
 * Frees the resources allocated by luxor_device_create().
 *
 * Since: 0.0
 */
void
luxor_device_destroy(luxor_device_t *device) {
  free(device);
}

