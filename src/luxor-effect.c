/*
 * luxor-effect.c: A effect
 *
 * Copyright © 2018 Bryce Harrington
 *
 * Released under mit, read the file 'COPYING' for more information.
 *
 * Contributors:
 */

/*
 * This file defines ...
 * It is based on ...
 * It is used by ...
 */
/**
 * SECTION:luxor-effect
 * @title: 
 * @short_description:  type
 *
 * #luxor_effect_t is a type representing...
 */

#include "luxor-effect-private.h"

/**
 * luxor_effect_create:
 *
 * Allocates a new #luxor_effect_t.
 *
 * The contents of the returned effect are undefined.
 *
 * @returns The newly allocated effect, or @c NULL on error.
 *
 * Since: 0.0
 */
luxor_effect_t *
luxor_effect_create(void) {
  struct luxor_effect_t *effect;

  effect = calloc(1, sizeof (*effect));
  if (effect == NULL)
    return NULL;

  return effect;
}

/**
 * luxor_effect_init:
 * @effect: A #luxor_effect_t to be initialized.
 *
 * Since: 0.0
 */
void
luxor_effect_init(luxor_effect_t *effect) {
  /* TODO: Initialize the structure's values */
}

/**
 * luxor_effect_destroy:
 * @effect: A #luxor_effect_t to be freed.
 *
 * Frees the resources allocated by luxor_effect_create().
 *
 * Since: 0.0
 */
void
luxor_effect_destroy(luxor_effect_t *effect) {
  free(effect);
}

