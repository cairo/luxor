/*
 * luxor-surface-private.h: A surface
 *
 * Copyright © 2018 Bryce Harrington
 *
 * Released under mit, read the file 'COPYING' for more information.
 *
 * Contributors:
 */

/*
 * This file defines ...
 * It is based on ...
 * It is used by ...
 */
#ifndef LUXOR_SURFACE_H
#define LUXOR_SURFACE_H

/**
 * luxor_surface_t:
 *
 * Since: 0.0
 */
typedef struct _luxor_surface {
  // TODO
} luxor_surface_t;

#endif /* LUXOR_SURFACE_H */
