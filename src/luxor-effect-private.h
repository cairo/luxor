/*
 * luxor-effect-private.h: A effect
 *
 * Copyright © 2018 Bryce Harrington
 *
 * Released under mit, read the file 'COPYING' for more information.
 *
 * Contributors:
 */

/*
 * This file defines ...
 * It is based on ...
 * It is used by ...
 */
#ifndef LUXOR_EFFECT_H
#define LUXOR_EFFECT_H

/**
 * luxor_effect_t:
 *
 * Since: 0.0
 */
typedef struct _luxor_effect {
  // TODO
} luxor_effect_t;

#endif /* LUXOR_EFFECT_H */
