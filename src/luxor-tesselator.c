/*
 * luxor-tesselator.c: A tesselator
 *
 * Copyright © 2018 Bryce Harrington
 *
 * Released under mit, read the file 'COPYING' for more information.
 *
 * Contributors:
 */

/*
 * This file defines ...
 * It is based on ...
 * It is used by ...
 */
/**
 * SECTION:luxor-tesselator
 * @title: 
 * @short_description:  type
 *
 * #luxor_tesselator_t is a type representing...
 */

#include "luxor-tesselator-private.h"

/**
 * luxor_tesselator_create:
 *
 * Allocates a new #luxor_tesselator_t.
 *
 * The contents of the returned tesselator are undefined.
 *
 * @returns The newly allocated tesselator, or @c NULL on error.
 *
 * Since: 0.0
 */
luxor_tesselator_t *
luxor_tesselator_create(void) {
  struct luxor_tesselator_t *tesselator;

  tesselator = calloc(1, sizeof (*tesselator));
  if (tesselator == NULL)
    return NULL;

  return tesselator;
}

/**
 * luxor_tesselator_init:
 * @tesselator: A #luxor_tesselator_t to be initialized.
 *
 * Since: 0.0
 */
void
luxor_tesselator_init(luxor_tesselator_t *tesselator) {
  /* TODO: Initialize the structure's values */
}

/**
 * luxor_tesselator_destroy:
 * @tesselator: A #luxor_tesselator_t to be freed.
 *
 * Frees the resources allocated by luxor_tesselator_create().
 *
 * Since: 0.0
 */
void
luxor_tesselator_destroy(luxor_tesselator_t *tesselator) {
  free(tesselator);
}

