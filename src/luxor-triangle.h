/*
 * luxor-triangle.h: A triangle
 *
 * Copyright © 2018 Bryce Harrington
 *
 * Released under mit, read the file 'COPYING' for more information.
 *
 * Contributors:
 */

/*
 * This file defines ...
 * It is based on ...
 * It is used by ...
 */
#ifndef LUXOR_TRIANGLE_H
#define LUXOR_TRIANGLE_H

luxor_triangle_t *
luxor_triangle_create();

void
luxor_triangle_init(luxor_triangle_t *triangle);

void
luxor_triangle_destroy(luxor_triangle_t *triangle);

#endif /* LUXOR_TRIANGLE_H */
