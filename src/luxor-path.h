/*
 * luxor-path.h: A path
 *
 * Copyright © 2018 Bryce Harrington
 *
 * Released under mit, read the file 'COPYING' for more information.
 *
 * Contributors:
 */

/*
 * This file defines ...
 * It is based on ...
 * It is used by ...
 */
#ifndef LUXOR_PATH_H
#define LUXOR_PATH_H

luxor_path_t *
luxor_path_create();

void
luxor_path_init(luxor_path_t *path);

void
luxor_path_destroy(luxor_path_t *path);

#endif /* LUXOR_PATH_H */
