/*
 * luxor-triangle.c: A triangle
 *
 * Copyright © 2018 Bryce Harrington
 *
 * Released under mit, read the file 'COPYING' for more information.
 *
 * Contributors:
 */

/*
 * This file defines ...
 * It is based on ...
 * It is used by ...
 */
/**
 * SECTION:luxor-triangle
 * @title: 
 * @short_description:  type
 *
 * #luxor_triangle_t is a type representing...
 */

#include "luxor-triangle-private.h"

/**
 * luxor_triangle_create:
 *
 * Allocates a new #luxor_triangle_t.
 *
 * The contents of the returned triangle are undefined.
 *
 * @returns The newly allocated triangle, or @c NULL on error.
 *
 * Since: 0.0
 */
luxor_triangle_t *
luxor_triangle_create(void) {
  struct luxor_triangle_t *triangle;

  triangle = calloc(1, sizeof (*triangle));
  if (triangle == NULL)
    return NULL;

  return triangle;
}

/**
 * luxor_triangle_init:
 * @triangle: A #luxor_triangle_t to be initialized.
 *
 * Since: 0.0
 */
void
luxor_triangle_init(luxor_triangle_t *triangle) {
  /* TODO: Initialize the structure's values */
}

/**
 * luxor_triangle_destroy:
 * @triangle: A #luxor_triangle_t to be freed.
 *
 * Frees the resources allocated by luxor_triangle_create().
 *
 * Since: 0.0
 */
void
luxor_triangle_destroy(luxor_triangle_t *triangle) {
  free(triangle);
}

