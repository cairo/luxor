/*
 * luxor-stream.c: A stream
 *
 * Copyright © 2018 Bryce Harrington
 *
 * Released under mit, read the file 'COPYING' for more information.
 *
 * Contributors:
 */

/*
 * This file defines ...
 * It is based on ...
 * It is used by ...
 */
/**
 * SECTION:luxor-stream
 * @title: 
 * @short_description:  type
 *
 * #luxor_stream_t is a type representing...
 */

#include "luxor-stream-private.h"

/**
 * luxor_stream_create:
 *
 * Allocates a new #luxor_stream_t.
 *
 * The contents of the returned stream are undefined.
 *
 * @returns The newly allocated stream, or @c NULL on error.
 *
 * Since: 0.0
 */
luxor_stream_t *
luxor_stream_create(void) {
  struct luxor_stream_t *stream;

  stream = calloc(1, sizeof (*stream));
  if (stream == NULL)
    return NULL;

  return stream;
}

/**
 * luxor_stream_init:
 * @stream: A #luxor_stream_t to be initialized.
 *
 * Since: 0.0
 */
void
luxor_stream_init(luxor_stream_t *stream) {
  /* TODO: Initialize the structure's values */
}

/**
 * luxor_stream_destroy:
 * @stream: A #luxor_stream_t to be freed.
 *
 * Frees the resources allocated by luxor_stream_create().
 *
 * Since: 0.0
 */
void
luxor_stream_destroy(luxor_stream_t *stream) {
  free(stream);
}

