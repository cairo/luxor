/*
 * luxor-device.h: A device
 *
 * Copyright © 2018 Bryce Harrington
 *
 * Released under mit, read the file 'COPYING' for more information.
 *
 * Contributors:
 */

/*
 * This file defines ...
 * It is based on ...
 * It is used by ...
 */
#ifndef LUXOR_DEVICE_H
#define LUXOR_DEVICE_H

luxor_device_t *
luxor_device_create();

void
luxor_device_init(luxor_device_t *device);

void
luxor_device_destroy(luxor_device_t *device);

#endif /* LUXOR_DEVICE_H */
