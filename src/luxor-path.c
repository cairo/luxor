/*
 * luxor-path.c: A path
 *
 * Copyright © 2018 Bryce Harrington
 *
 * Released under mit, read the file 'COPYING' for more information.
 *
 * Contributors:
 */

/*
 * This file defines ...
 * It is based on ...
 * It is used by ...
 */
/**
 * SECTION:luxor-path
 * @title: 
 * @short_description:  type
 *
 * #luxor_path_t is a type representing...
 */

#include "luxor-path-private.h"

/**
 * luxor_path_create:
 *
 * Allocates a new #luxor_path_t.
 *
 * The contents of the returned path are undefined.
 *
 * @returns The newly allocated path, or @c NULL on error.
 *
 * Since: 0.0
 */
luxor_path_t *
luxor_path_create(void) {
  struct luxor_path_t *path;

  path = calloc(1, sizeof (*path));
  if (path == NULL)
    return NULL;

  return path;
}

/**
 * luxor_path_init:
 * @path: A #luxor_path_t to be initialized.
 *
 * Since: 0.0
 */
void
luxor_path_init(luxor_path_t *path) {
  /* TODO: Initialize the structure's values */
}

/**
 * luxor_path_destroy:
 * @path: A #luxor_path_t to be freed.
 *
 * Frees the resources allocated by luxor_path_create().
 *
 * Since: 0.0
 */
void
luxor_path_destroy(luxor_path_t *path) {
  free(path);
}

