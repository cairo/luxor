/*
 * luxor-font.c: A font
 *
 * Copyright © 2018 Bryce Harrington
 *
 * Released under mit, read the file 'COPYING' for more information.
 *
 * Contributors:
 */

/*
 * This file defines ...
 * It is based on ...
 * It is used by ...
 */
/**
 * SECTION:luxor-font
 * @title: 
 * @short_description:  type
 *
 * #luxor_font_t is a type representing...
 */

#include "luxor-font-private.h"

/**
 * luxor_font_create:
 *
 * Allocates a new #luxor_font_t.
 *
 * The contents of the returned font are undefined.
 *
 * @returns The newly allocated font, or @c NULL on error.
 *
 * Since: 0.0
 */
luxor_font_t *
luxor_font_create(void) {
  struct luxor_font_t *font;

  font = calloc(1, sizeof (*font));
  if (font == NULL)
    return NULL;

  return font;
}

/**
 * luxor_font_init:
 * @font: A #luxor_font_t to be initialized.
 *
 * Since: 0.0
 */
void
luxor_font_init(luxor_font_t *font) {
  /* TODO: Initialize the structure's values */
}

/**
 * luxor_font_destroy:
 * @font: A #luxor_font_t to be freed.
 *
 * Frees the resources allocated by luxor_font_create().
 *
 * Since: 0.0
 */
void
luxor_font_destroy(luxor_font_t *font) {
  free(font);
}

