/*
 * luxor-mimedata-private.h: A mimedata
 *
 * Copyright © 2018 Bryce Harrington
 *
 * Released under mit, read the file 'COPYING' for more information.
 *
 * Contributors:
 */

/*
 * This file defines ...
 * It is based on ...
 * It is used by ...
 */
#ifndef LUXOR_MIMEDATA_H
#define LUXOR_MIMEDATA_H

/**
 * luxor_mimedata_t:
 *
 * Since: 0.0
 */
typedef struct _luxor_mimedata {
  // TODO
} luxor_mimedata_t;

#endif /* LUXOR_MIMEDATA_H */
