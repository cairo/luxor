/*
 * luxor-region-private.h: A region
 *
 * Copyright © 2018 Bryce Harrington
 *
 * Released under mit, read the file 'COPYING' for more information.
 *
 * Contributors:
 */

/*
 * This file defines ...
 * It is based on ...
 * It is used by ...
 */
#ifndef LUXOR_REGION_H
#define LUXOR_REGION_H

/**
 * luxor_region_t:
 *
 * Since: 0.0
 */
typedef struct _luxor_region {
  // TODO
} luxor_region_t;

#endif /* LUXOR_REGION_H */
