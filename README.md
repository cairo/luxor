Luxor Vector Graphics Library
------------------------------------------------------------------------

**Luxor** is a 2D vector graphics drawing API using
[Vulkan](https://www.khronos.org/vulkan/).

 * ...
 * ...

Luxor is provided under the MIT license.

For more information, see ...

### Building

Building Luxor is fairly straightforward:

    $ git clone <git repository>
    $ cd luxor
    $ mkdir build && cd build
    $ meson
    $ ninja
    $ sudo ninja install

### Dependencies

 * ...
 * ...

