== Todo list for LUXOR ==

* Fill in README.md
  + Key points
  + Dependencies
* Add other source code files to src/meson.build
* Flesh out main.c
* docs: Fill in high level docs in doc/doxygen/mainpage.dox
* docs: To customize the appearance, this generates the default
  stylesheet, header, and footer files for editing:
  + HTML:
    doxygen -w html header.html footer.html stylesheet.css <config_file>
  + Latex:
    doxygen -w latex header.tex footer.tex doxygen.sty <config_file>
* Write test cases using cmocka
* Verify meson build works
* Set up git repository on gitlab, add link to README.md

